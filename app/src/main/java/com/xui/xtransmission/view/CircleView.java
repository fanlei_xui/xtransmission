package com.xui.xtransmission.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.xui.xtransmission.R;


public class CircleView extends View {

	private Context mContext;
	private Paint paint;
	private int angle = 0;
	private final int ringWidth = 4;
	private Bitmap circleBg;
	private Bitmap pointBg;

	public CircleView(Context context) {
		super(context);
	}

	public CircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
		this.paint = new Paint();
		this.paint.setAntiAlias(true); // 消除锯齿
		this.paint.setStyle(Paint.Style.STROKE); // 绘制空心圆
		circleBg = BitmapFactory.decodeResource(getContext().getResources(),
				R.drawable.circle_bg);
		pointBg = BitmapFactory.decodeResource(getContext().getResources(),
				R.drawable.pointer);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (circleBg != null) {
			canvas.drawBitmap(circleBg, (getWidth() - circleBg.getWidth()) / 2,
					(getHeight() - circleBg.getHeight()) / 2, paint);
		}

		if (pointBg != null) {
			canvas.drawBitmap(pointBg, (getWidth() - pointBg.getWidth()) / 2,
					(getHeight() - pointBg.getHeight()) / 2, paint);
		}

		this.paint.setARGB(255, 136, 178, 228);
		this.paint.setStrokeWidth(ringWidth);

		canvas.drawArc(
				new RectF((getWidth() - circleBg.getWidth()) / 2 + ringWidth / 2,
						(getHeight() - circleBg.getHeight()) / 2 + ringWidth / 2,
						(getWidth() + circleBg.getWidth()) / 2 - ringWidth / 2,
						(getHeight() + circleBg.getHeight()) / 2 - ringWidth / 2), 270, angle,
				false, paint);
		angle += 3;
		if (angle == 360){
			angle = 0;
		}
		invalidate();
	}

	/**
	 * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
	 */
	public static int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}
}
