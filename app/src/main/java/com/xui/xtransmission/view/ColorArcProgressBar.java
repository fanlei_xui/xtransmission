package com.xui.xtransmission.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.xui.xtransmission.R;


public class ColorArcProgressBar extends View implements ValueAnimator.AnimatorUpdateListener, Animator.AnimatorListener {

    private int mWidth;  // 视图宽度
    private int mHeight; // 视图高度

    //圆心
    private float centerX;
    private float centerY;
    //左边点坐标
    private float left_x;
    private float left_y;
    //右边点的坐标
    private float right_x;
    private float right_y;

    private Paint allArcPaint;  // 背景画笔
    private Paint progressPaint;// 进度画笔
    private Paint decoratePaint;// 装饰画笔

    private RectF bgRect;                    // 弧形所在区域
    private RectF deRect;                    // 装饰弧所在区域
    private final float startAngle = 135;    // 起始角度
    private final float sweepAngle = 270;    // 圆弧角度(定值)

    private boolean USE_CENTER = false;      // 是否显示半径线

    private float bgArcWidth = dipToPx(20);  // 背景弧的宽度
    private int bgArcColor = Color.BLACK;    // 背景色

    private float progressWidth = dipToPx(20);// 进度的宽度
    private int progressColor = Color.WHITE;  // 进度背景色

    private float decorateWidth = dipToPx(1); // 装饰弧的宽度
    private int decorateColor = Color.WHITE;

    private float currentAngle = 0;           // 当前进度值(0 - 最大值)

    private float maxProgress = 100;        // 最大值(默认100)

    private float RATE = sweepAngle / maxProgress;

    private float endValue = 0;//动画的endValue

    private ValueAnimator animator;


    public ColorArcProgressBar(Context context) {
        super(context, null);
        initPaint();
    }

    public ColorArcProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        initAttrs(attrs);
        initPaint();
    }

    public ColorArcProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        initPaint();

    }

    //初始化属性
    private void initAttrs(AttributeSet attrs) {
        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.ColorArcProgressBar);
        bgArcWidth = array.getFloat(R.styleable.ColorArcProgressBar_ArcWidth, dipToPx(20));       // 背景弧形宽度
        progressWidth = array.getFloat(R.styleable.ColorArcProgressBar_ArcWidth, dipToPx(21));    // 进度弧形宽度
        decorateWidth = array.getFloat(R.styleable.ColorArcProgressBar_decorateWidth, dipToPx(1)); // 装饰弧的宽度

        bgArcColor = array.getColor(R.styleable.ColorArcProgressBar_bgArcColor, Color.BLACK);     // 背景色
        progressColor = array.getColor(R.styleable.ColorArcProgressBar_progressColor, Color.BLUE);// 进度条颜色
        decorateColor = array.getColor(R.styleable.ColorArcProgressBar_progressColor, Color.BLUE);// 装饰条颜色

        maxProgress = array.getFloat(R.styleable.ColorArcProgressBar_maxProgress, 100);           // 最大值（默认100）
        array.recycle();
    }

    //初始化画笔
    private void initPaint() {

        //整个弧形
        allArcPaint = new Paint();
        allArcPaint.setAntiAlias(true);
        allArcPaint.setStyle(Paint.Style.STROKE);
        allArcPaint.setStrokeWidth(bgArcWidth);
        allArcPaint.setColor(bgArcColor);
        allArcPaint.setStrokeCap(Paint.Cap.ROUND);

        //当前进度的弧形
        progressPaint = new Paint();
        progressPaint.setAntiAlias(true);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setStrokeCap(Paint.Cap.ROUND);
        progressPaint.setStrokeWidth(progressWidth);
        progressPaint.setColor(progressColor);

        //装饰的弧形
        decoratePaint = new Paint();
        decoratePaint.setAntiAlias(true);
        decoratePaint.setStyle(Paint.Style.STROKE);
        decoratePaint.setStrokeCap(Paint.Cap.ROUND);
        decoratePaint.setStrokeWidth(decorateWidth);
        decoratePaint.setColor(decorateColor);
    }

    //计算弧形区域
    private void CalculationRectF() {

        //===================弧形的区域======================//
        bgRect = new RectF();
        //左上角坐标
        bgRect.left = centerX - Math.min(centerX, centerY) + bgArcWidth + bgArcWidth / 2;
        bgRect.top = centerY - Math.min(centerX, centerY) + bgArcWidth + bgArcWidth / 2;
        //右下角坐标
        bgRect.right = centerX + Math.min(centerX, centerY) - bgArcWidth - bgArcWidth / 2;
        bgRect.bottom = centerY + Math.min(centerX, centerY) - bgArcWidth - bgArcWidth / 2;

        Log.d("ColorArcProgressBar", bgRect.toShortString());


        //==================装饰弧的区域=====================//
        deRect = new RectF();
        //左上角坐标
        deRect.left = centerX - Math.min(centerX, centerY) + bgArcWidth / 2;
        deRect.top = centerY - Math.min(centerX, centerY) + bgArcWidth / 2;
        //右下角坐标
        deRect.right = centerX + Math.min(centerX, centerY) - bgArcWidth / 2;
        deRect.bottom = centerY + Math.min(centerX, centerY) - bgArcWidth / 2;

        Log.d("ColorArcProgressBar", deRect.toShortString());
    }

    //计算圆心
    private void CalculationCenterXY() {
        centerX = mWidth / 2;
        centerY = mHeight / 2;
    }

    //计算装饰圆弧的两点坐标
    private void CalculationDecorateXY() {

        left_x = (float) (centerX - ((deRect.width() / 2 / Math.sqrt(2))));
        left_y = (float) (centerY + ((deRect.width() / 2 / Math.sqrt(2))));

        right_x = (float) (centerX + ((deRect.width() / 2 / Math.sqrt(2))));
        right_y = (float) (centerY + ((deRect.width() / 2 / Math.sqrt(2))));

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = getWidth();  // 视图宽度
        mHeight = getHeight();// 视图高度

        Log.d("ColorArcProgressBar", "mWidth:" + mWidth);
        Log.d("ColorArcProgressBar", "mHeight:" + mHeight);

        CalculationCenterXY();// 计算圆心

        CalculationRectF();   // 计算弧形区域

        CalculationDecorateXY();//装饰两点的坐标
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {

        //抗锯齿
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));

        //整个弧
        canvas.drawArc(bgRect, startAngle, sweepAngle, USE_CENTER, allArcPaint);

        //当前进度
        canvas.drawArc(bgRect, startAngle, currentAngle, USE_CENTER, progressPaint);

        //外层装饰
        canvas.drawArc(deRect, startAngle, sweepAngle, USE_CENTER, decoratePaint);

        //外层装饰的横线
        canvas.drawLine(left_x, left_y, left_x - 20, left_y, decoratePaint);
        canvas.drawLine(right_x, right_y, right_x + 20, right_y, decoratePaint);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (animator != null) {
            animator.cancel();
            animator = null;
        }
    }

    //得到当前进度
    public float getCurrentAngle() {
        return currentAngle / RATE;
    }

    //设置当前进度
    public void setCurrentAngle(float currentAngle) {
        this.currentAngle = currentAngle * RATE;
        invalidate();
    }

    //设置圆弧宽度
    public void setBgArcWidth(int bgArcWidth) {
        this.bgArcWidth = bgArcWidth;
        this.progressWidth = this.bgArcWidth;
    }

    //dip 转换成px
    private int dipToPx(float dip) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f * (dip >= 0 ? 1 : -1));
    }

    //设置最大值
    public void setMaxProgress(float maxProgress) {
        this.maxProgress = maxProgress;
        RATE = sweepAngle / maxProgress;
    }

    //获取当前进度
    public float getMaxProgress() {
        return maxProgress;
    }

    //进度更新
    @Override
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.currentAngle = (float) valueAnimator.getAnimatedValue();
        invalidate();
    }

    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {
        this.currentAngle = endValue;
        invalidate();
    }

    @Override
    public void onAnimationCancel(Animator animator) {
        this.currentAngle = endValue;
        invalidate();
    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }
}
