package com.xui.xtransmission.app;

import android.app.Application;

import com.tcstudio.xcloudcsdk.logic.db.dao.ConfigDao;
import com.tcstudio.xcloudcsdk.logic.util.Utils;


/**
 * Created by fanlei on 2016/12/5.
 */
public class XTransApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        init();

    }

    private void init() {

        Utils.mContext = getApplicationContext();
        ConfigDao.getInstance(getApplicationContext()).initConfig();

    }
}
