package com.xui.xtransmission;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.tcstudio.xcloudcsdk.logic.db.dao.AppDao;
import com.tcstudio.xcloudcsdk.logic.db.dao.CarBindDao;
import com.tcstudio.xcloudcsdk.logic.db.dao.MapPkgDao;
import com.tcstudio.xcloudcsdk.logic.listener.OnBuildApListener;
import com.tcstudio.xcloudcsdk.logic.manager.IWifiManager;
import com.tcstudio.xcloudcsdk.logic.service.BaseService;
import com.tcstudio.xcloudcsdk.logic.util.LogUtil;
import com.xui.xtransmission.base.BaseActivity;
import com.xui.xtransmission.fragment.DiskFullDialogFragment;
import com.xui.xtransmission.fragment.NoBinddingDialogFragment;
import com.xui.xtransmission.fragment.OkTransProgressFragment;
import com.xui.xtransmission.fragment.OkTransReceiveSuccessFragment;
import com.xui.xtransmission.fragment.StartWifiFragment;
import com.xui.xtransmission.fragment.TransingApErrorFragment;
import com.xui.xtransmission.fragment.UnCompleteTaskDialogFragment;
import com.xui.xtransmission.fragment.UnzipMapProgressDialogFragment;
import com.xui.xtransmission.service.CarService;

public class MainActivity extends BaseActivity implements View.OnClickListener, StartWifiFragment.OnStartWifiFragmentListener,
        DiskFullDialogFragment.OnDiskFullListener, UnzipMapProgressDialogFragment.OnUnzipMapProgressListener,
        UnCompleteTaskDialogFragment.OnUnCompleteTaskListener, OkTransReceiveSuccessFragment.OnOkTransReceiveSuccessListener,
        NoBinddingDialogFragment.OnNoBinddingDialogListener {

    private static final String tag = "MainActivity";

    private static final int ACTION_ON_FADE = 10000;

    private ServerConnection mConnection;
    private CarService carService;

    private FragmentManager fm;//fragment的管理者
    private FragmentTransaction ft;//fragment的事务

    private StartWifiFragment startWifiFragment;//开启WiFi的fragment
    private OkTransProgressFragment okTransProgressFragment;//传输进度Fragment
    private OkTransReceiveSuccessFragment okTransReceiveSuccessFragment;//接收完成
    private TransingApErrorFragment transingApErrorFragment;//传输错误Fragment
    private UnzipMapProgressDialogFragment unzipMapProgressDialogFragment;//解压地图的Fragment
    private UnCompleteTaskDialogFragment unCompleteTaskDialogFragment;//未完成

    private DiskFullDialogFragment diskFullDialogFragment;//内存不足的弹窗

    private NoBinddingDialogFragment noBinddingDialogFragment;//未绑定的Dialog

    private boolean isFirst = true;
    private static boolean isTopTask = true;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @SuppressLint("CommitTransaction")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CarService.ACTION_ON_OKT_RECEIVE_START:// 一键传输
                case CarService.ACTION_ON_MT_RECEIVE_START: // 音乐列表传输
                    LogUtil.i(tag, "开始接收");

                    ft = fm.beginTransaction();
                    okTransProgressFragment = OkTransProgressFragment.newInstance();
                    ft.replace(R.id.fragment_container, okTransProgressFragment);
                    ft.commitAllowingStateLoss();

                    break;
                case CarService.ACTION_ON_MT_RECEIVE_PROGRESS:  // 音乐列表进度
                case CarService.ACTION_ON_OKT_RECEIVE_PROGREESS:// 一键传输进度
                    LogUtil.i(tag, "正在接收...");

                    int progress = msg.arg1;
                    int fileType = msg.arg2;
                    String fileName = (String) msg.obj;

                    if (okTransProgressFragment != null) {
                        okTransProgressFragment.setOkTransProgressStatus(progress, fileType, fileName);
                    } else {
                        ft = fm.beginTransaction();
                        okTransProgressFragment = OkTransProgressFragment.newInstance();
                        ft.replace(R.id.fragment_container, okTransProgressFragment, "receive_progress");
                        ft.commitAllowingStateLoss();
                    }

                    break;
                case CarService.ACTION_ON_MT_RECEIVE_OVER:
                case CarService.ACTION_ON_OKT_RECEIVE_OVER://传输完成

                    okTransProgressFragment.setOkTransComplete();

                    Bundle bundle = (Bundle) msg.obj;
                    okTransReceiveSuccessFragment = OkTransReceiveSuccessFragment.newInstance(bundle);
                    okTransReceiveSuccessFragment.setOnOkTransReceiveSuccessListener(MainActivity.this);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ft = fm.beginTransaction();
                            ft.replace(R.id.fragment_container, okTransReceiveSuccessFragment, "receive_success");
                            ft.commitAllowingStateLoss();
                        }
                    }, 1000);

                    IWifiManager.getInstance(getApplicationContext()).stopWifiAp();

                    break;
                case CarService.ACTION_ON_TRANSFER_DISK_FULL://传输过程中出错

                    LogUtil.d("fanlei", "传输异常");

                    bundle = (Bundle) msg.obj;

                    ft = fm.beginTransaction();
                    transingApErrorFragment = TransingApErrorFragment.newInstance(bundle);
                    ft.replace(R.id.fragment_container, transingApErrorFragment, "receive_error_disk_full");
                    ft.commitAllowingStateLoss();

                    Bundle error2 = new Bundle();
                    error2.putString("title", "传输异常");
                    error2.putInt("needSize", 123);
                    diskFullDialogFragment = DiskFullDialogFragment.newInstance(error2);
                    diskFullDialogFragment.setOnDiskFullListener(MainActivity.this);
                    diskFullDialogFragment.show(fm, "disk_full");

                    IWifiManager.getInstance(getApplicationContext()).stopWifiAp();

                    break;
                case CarService.ACTION_ON_PROGRESS_EXCEPTION://进度异常

                    LogUtil.d("fanlei", "进度异常");

                    bundle = (Bundle) msg.obj;

                    ft = fm.beginTransaction();
                    transingApErrorFragment = TransingApErrorFragment.newInstance(bundle);
                    ft.replace(R.id.fragment_container, transingApErrorFragment, "receive_error");
                    ft.commitAllowingStateLoss();

                    IWifiManager.getInstance(getApplicationContext()).stopWifiAp();

                    break;
                case CarService.ACTION_ON_UNZIP_START://开始解压

                    if (unzipMapProgressDialogFragment == null) {
                        unzipMapProgressDialogFragment = UnzipMapProgressDialogFragment.newInstance();
                    }

                    if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() == null) {
                        unzipMapProgressDialogFragment.show(fm, "unzip_map_progress");
                    }

                    break;
                case CarService.ACTION_ON_UNZIP_PROGRESS://解压进度

                    progress = msg.arg1;
                    String cityName = (String) msg.obj;

                    if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                            && unzipMapProgressDialogFragment.getDialog().isShowing()) {
                        unzipMapProgressDialogFragment.setUnzipMapProgress(progress, cityName);//设置进度和城市名
                    }

                    break;
                case CarService.ACTION_ON_UNZIP_DISK_FULL_ERROR://解压出错(磁盘空间不足)

                    int needSize = msg.arg1;//所需大小

                    //取消原有的
                    if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                            && unzipMapProgressDialogFragment.getDialog().isShowing()) {
                        unzipMapProgressDialogFragment.dismiss();
                    }

                    Bundle error = new Bundle();
                    error.putInt("needSize", needSize);
                    error.putString("title", "解压出错");
                    diskFullDialogFragment = DiskFullDialogFragment.newInstance(error);
                    diskFullDialogFragment.setOnDiskFullListener(MainActivity.this);
                    diskFullDialogFragment.show(fm, "disk_full");

                    break;
                case CarService.ACTION_ON_UNZIP_UNZIP_ERROR://解压错误
                    //取消原有的
                    if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                            && unzipMapProgressDialogFragment.getDialog().isShowing()) {
                        unzipMapProgressDialogFragment.dismiss();
                    }

                    Bundle unzip_error = new Bundle();
                    unzip_error.putString("title", "解压错误");
                    unzip_error.putInt("needSize", 111);
                    diskFullDialogFragment = DiskFullDialogFragment.newInstance(unzip_error);
                    diskFullDialogFragment.setOnDiskFullListener(MainActivity.this);
                    diskFullDialogFragment.show(fm, "disk_full");

                    break;
                case CarService.ACTION_ON_UNZIP_OTHER_ERROR://其它错误

                    //取消原有的
                    if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                            && unzipMapProgressDialogFragment.getDialog().isShowing()) {
                        unzipMapProgressDialogFragment.dismiss();
                    }

                    Bundle error1 = new Bundle();
                    error1.putString("title", "未知错误");
                    error1.putInt("needSize", 111);
                    diskFullDialogFragment = DiskFullDialogFragment.newInstance(error1);
                    diskFullDialogFragment.setOnDiskFullListener(MainActivity.this);
                    diskFullDialogFragment.show(fm, "disk_full");

                    break;

                case CarService.ACTION_ON_UNZIP_COMPLETE://解压完成

                    if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                            && unzipMapProgressDialogFragment.getDialog().isShowing()) {

                        unzipMapProgressDialogFragment.setUnzipMapSuccess();

                    }

                    break;
                case CarService.ACTION_ON_OPTIMIZ_START:   // 优化内存

                    break;
                case CarService.ACTION_ON_OPTIMIZ_PROGRESS:// 优化进度

                    break;
                case CarService.ACTION_ON_OPTIMIZ_SUCCESS: // 优化成功

                    break;
                case CarService.ACTION_ON_OPTIMIZ_ERROR:   // 优化失败

                    break;
                case ACTION_ON_FADE:

                    break;
                default:
                    break;
            }
        }
    };

    @SuppressLint("CommitTransaction")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CarBindDao.getInstance(getApplicationContext()).isBinded()) {
            setContentView(R.layout.activity_main);
            init();
        } else {
            setContentView(R.layout.activity_main);
            noBinddingDialogFragment = NoBinddingDialogFragment.newInstance();
            noBinddingDialogFragment.setOnNoBinddingDialogListener(this);
            noBinddingDialogFragment.show(getSupportFragmentManager(), "no_bindding");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isTopTask = true;
    }

    @Override
    protected void onPause() {
        if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                && unzipMapProgressDialogFragment.getDialog().isShowing()) {
            unzipMapProgressDialogFragment.dismiss();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (unzipMapProgressDialogFragment != null && unzipMapProgressDialogFragment.getDialog() != null
                && unzipMapProgressDialogFragment.getDialog().isShowing()) {
            unzipMapProgressDialogFragment.dismiss();
        }
        super.onStop();
        isTopTask = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnection != null) {
            unbindService(mConnection);
        }
        IWifiManager.getInstance(getApplicationContext()).stopWifiAp();
    }


    //判断是否有未完成的任务~(未安装的应用或者是未解压的地图包)
    private boolean isHaveUncompleteTask() {
        if (AppDao.getInstance(getApplicationContext()).isHaveUninstallApp()
                || MapPkgDao.getInstance(getApplicationContext()).isHaveUnzipMapPkg()) {
            return true;
        } else {
            return false;
        }
    }

    private void startWifiAp() {
        IWifiManager.getInstance(getApplicationContext()).startWifiAp(new OnBuildApListener() {

            @Override
            public void onBuildApSuccess(String apName, String apPwd) {

                bindService();

                handler.post(new Runnable() {

                    @Override
                    public void run() {

                        startWifiFragment.setWifiFragmentState(2);//开启成功

                    }
                });
            }

            @Override
            public void onBuildApFailed(IWifiManager.WifiErrorCode errorCode) {

                startWifiFragment.setWifiFragmentState(1);//开启失败

            }
        });
    }

    //初始化的操作
    @SuppressLint("CommitTransaction")
    private void init() {
        if (CarBindDao.getInstance(getApplicationContext()).isBinded()) {
            if (isHaveUncompleteTask() && isFirst) {
                unCompleteTaskDialogFragment = new UnCompleteTaskDialogFragment();
                unCompleteTaskDialogFragment.setOnUnCompleteTaskListener(this);
                unCompleteTaskDialogFragment.show(getSupportFragmentManager(), "unComplete");
            }
            isFirst = false;

            fm = getSupportFragmentManager();
            startWifiFragment = StartWifiFragment.newInstance();
            ft = fm.beginTransaction();
            ft.replace(R.id.fragment_container, startWifiFragment, "startWiFi");
            ft.commitAllowingStateLoss();
            startWifiFragment.setOnRestartWifiListener(this);//监听

            startWifiAp();
        }
    }

    //开启服务(绑定形式打开服务)
    private void bindService() {
        LogUtil.i(tag, "bindService");
        mConnection = new ServerConnection();
        Intent intent = new Intent(this, CarService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    //点击重新开启WiFi的按钮的监听
    @Override
    public void onRestartWifiListener() {
        startWifiFragment.setWifiFragmentState(0);
        startWifiAp();
    }

    //内存不足的弹窗点击回调
    @Override
    public void onDiskFullListener() {
        diskFullDialogFragment.dismiss();
    }

    //解压进度的回调
    @Override
    public void onUnzipMapProgressListener() {

    }

    //未完成的任务的回调
    @Override
    public void onUnCompleteTaskListener(boolean select) {
        if (select) {
            unCompleteTaskDialogFragment.dismiss();
            carService.executeTask();
        } else {
            unCompleteTaskDialogFragment.dismiss();
        }
    }

    //传输完成后，点击继续传输的回调
    @Override
    public void onOkTransReceiveSuccessListener() {
        init();
    }

    //点击事件
    @Override
    public void onClick(View view) {

    }

    //未绑定弹窗按键的监听
    @Override
    public void onUnzipMapProgressListener(boolean yesOrNo) {
        //是 true 否 false
        if (yesOrNo) {
            //跳转到绑定应用
            try {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.xui.xbinding", "com.xui.xbinding.activity.MainActivity"));
                intent.setAction(Intent.ACTION_VIEW);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "应用未安装", Toast.LENGTH_SHORT).show();
            }
            noBinddingDialogFragment.dismiss();
            finish();
        } else {
            //否退出
            noBinddingDialogFragment.dismiss();
            finish();
        }
    }


    private class ServerConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            BaseService.BaseBinder baseBinder = (BaseService.BaseBinder) binder;
            carService = (CarService) baseBinder.getService();
            if (carService != null) {
                carService.setHandler(handler);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }

    }

}
