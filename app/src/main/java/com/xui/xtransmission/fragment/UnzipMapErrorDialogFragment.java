package com.xui.xtransmission.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.xui.xtransmission.R;


public class UnzipMapErrorDialogFragment extends DialogFragment {

    public interface OnUnzipMapErrorListener {
        void onUnzipMapErrorListener();
    }

    private OnUnzipMapErrorListener mListener;

    public void setOnUnzipMapErrorListener(OnUnzipMapErrorListener mListener) {
        this.mListener = mListener;
    }

    public static UnzipMapErrorDialogFragment newInstance() {
        UnzipMapErrorDialogFragment fragment = new UnzipMapErrorDialogFragment();
        return fragment;
    }

    private TextView tv_unzip_map_error_ok;//确定

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.9f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.fragment_unzip_map_error_dialog, container, false);

        initView(view);
        initEvent();

        return view;
    }


    private void initView(View view){
        tv_unzip_map_error_ok = (TextView) view.findViewById(R.id.tv_unzip_map_error_ok);
    }

    private void initEvent(){
        tv_unzip_map_error_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null){
                    mListener.onUnzipMapErrorListener();
                }
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
