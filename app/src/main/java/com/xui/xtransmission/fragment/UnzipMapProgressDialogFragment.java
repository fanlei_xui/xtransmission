package com.xui.xtransmission.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.xui.xtransmission.R;
import com.xui.xtransmission.view.ColorArcProgressBar;


public class UnzipMapProgressDialogFragment extends DialogFragment {

    public interface OnUnzipMapProgressListener {
        void onUnzipMapProgressListener();
    }

    private OnUnzipMapProgressListener mListener;

    public void setOnUnzipMapProgressListener(OnUnzipMapProgressListener mListener) {
        this.mListener = mListener;
    }

    public static UnzipMapProgressDialogFragment newInstance() {
        UnzipMapProgressDialogFragment fragment = new UnzipMapProgressDialogFragment();
        return fragment;
    }

    private ColorArcProgressBar capb_progress_unzip_map;//进度条
    private ImageView iv_icon_unzip_map;   // logo
    private TextView tv_msg_unzip_map;     // 展示信息

    private ImageView iv_anim_unzip_map;
    private ImageView iv_icon_unzip_anim;  // 动画
    private AnimationDrawable animationDrawable;

    private TextView tv_fileName_unzip_map;// 文件名称


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.95f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);

        View view = inflater.inflate(R.layout.fragment_unzip_map_progress_dialog, container, false);

        initView(view);
        startAnim();
        return view;
    }


    private void initView(View view) {
        capb_progress_unzip_map = (ColorArcProgressBar) view.findViewById(R.id.capb_progress_unzip_map);
        iv_icon_unzip_map = (ImageView) view.findViewById(R.id.iv_icon_unzip_map);
        tv_msg_unzip_map = (TextView) view.findViewById(R.id.tv_msg_unzip_map);
        iv_anim_unzip_map = (ImageView) view.findViewById(R.id.iv_anim_unzip_map);
        tv_fileName_unzip_map = (TextView) view.findViewById(R.id.tv_fileName_unzip_map);
        iv_icon_unzip_anim = (ImageView) view.findViewById(R.id.iv_icon_unzip_anim);
    }

    private void startAnim() {
        animationDrawable = (AnimationDrawable) iv_anim_unzip_map.getDrawable();
        animationDrawable.start();
    }

    public void setUnzipMapProgress(int progress, String fileName) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(capb_progress_unzip_map.getCurrentAngle(), progress);
        valueAnimator.setDuration(300);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                capb_progress_unzip_map.setCurrentAngle((Float) animation.getAnimatedValue());
            }
        });
        valueAnimator.start();
        tv_fileName_unzip_map.setText(fileName);
    }

    //解压完成
    public void setUnzipMapSuccess() {

        iv_icon_unzip_map.setVisibility(View.INVISIBLE);

        final ObjectAnimator iv_icon_unzip_map_anim = ObjectAnimator.ofFloat(iv_icon_unzip_anim, "rotationY", 90, 0);
        iv_icon_unzip_map_anim.setInterpolator(new LinearInterpolator());
        iv_icon_unzip_map_anim.setDuration(600);
        iv_icon_unzip_map_anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                iv_icon_unzip_anim.setVisibility(View.VISIBLE);
            }
        });
        iv_icon_unzip_map_anim.start();

        tv_msg_unzip_map.setText("解压完成");
        if (animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
        iv_anim_unzip_map.setVisibility(View.GONE);
        tv_fileName_unzip_map.setVisibility(View.GONE);

        tv_fileName_unzip_map.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getDialog() != null && getDialog().isShowing()) {
                    dismiss();
                }
            }
        }, 1500);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }
}
