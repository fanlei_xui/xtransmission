package com.xui.xtransmission.fragment;

import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xui.xtransmission.R;


public class StartWifiFragment extends Fragment {

    public interface OnStartWifiFragmentListener {
        void onRestartWifiListener();
    }

    private OnStartWifiFragmentListener mListener;

    public void setOnRestartWifiListener(OnStartWifiFragmentListener mListener) {
        this.mListener = mListener;
    }

    public static StartWifiFragment newInstance() {
        StartWifiFragment fragment = new StartWifiFragment();
        return fragment;
    }


    private ImageView image_show_wifi_startWifiFragment;       // 动画ImageView
    private AnimationDrawable animationDrawable;

    private TextView layout_wifi_starting_startWifiFragment;   // 正在开启Wifi

    private LinearLayout layout_wifi_restart_startWifiFragment;// 重新开启Wifi
    private Button btn_wifi_restart_startWifiFragment;         // 重新开启Wifi

    private LinearLayout layout_wifi_success_startWifiFragment;// wifi开启成功


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_wifi, container, false);

        initView(view);
        initEvent();

        setWifiFragmentState(0);

        return view;
    }

    //初始化View
    private void initView(View view) {
        image_show_wifi_startWifiFragment = (ImageView) view.findViewById(R.id.image_show_wifi_startWifiFragment);
        layout_wifi_starting_startWifiFragment = (TextView) view.findViewById(R.id.layout_wifi_starting_startWifiFragment);
        layout_wifi_restart_startWifiFragment = (LinearLayout) view.findViewById(R.id.layout_wifi_restart_startWifiFragment);
        btn_wifi_restart_startWifiFragment = (Button) view.findViewById(R.id.btn_wifi_restart_startWifiFragment);
        layout_wifi_success_startWifiFragment = (LinearLayout) view.findViewById(R.id.layout_wifi_success_startWifiFragment);
        animationDrawable = (AnimationDrawable) image_show_wifi_startWifiFragment.getDrawable();
    }

    private void initEvent() {
        btn_wifi_restart_startWifiFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onRestartWifiListener();
                }
            }
        });
    }


    //设置状态
    public void setWifiFragmentState(int status) {

        switch (status) {
            case 0://正在开启Wifi
                image_show_wifi_startWifiFragment.setImageDrawable(animationDrawable);
                animationDrawable.start();

                layout_wifi_starting_startWifiFragment.setVisibility(View.VISIBLE);
                layout_wifi_restart_startWifiFragment.setVisibility(View.GONE);
                layout_wifi_success_startWifiFragment.setVisibility(View.GONE);

                break;
            case 1://开启失败，重新开启Wifi
                animationDrawable.stop();
                image_show_wifi_startWifiFragment.setImageResource(R.drawable.transmission_app_icon_wifi_error);

                layout_wifi_starting_startWifiFragment.setVisibility(View.GONE);
                layout_wifi_restart_startWifiFragment.setVisibility(View.VISIBLE);
                layout_wifi_success_startWifiFragment.setVisibility(View.GONE);

                ObjectAnimator animator_alpha_e = ObjectAnimator.ofFloat(layout_wifi_restart_startWifiFragment, "alpha", 0, 1f);
                animator_alpha_e.setDuration(1000);
                animator_alpha_e.setInterpolator(new LinearOutSlowInInterpolator());
                animator_alpha_e.start();

                break;
            case 2://开启成功
                animationDrawable.stop();
                image_show_wifi_startWifiFragment.setImageResource(R.drawable.transmission_app_icon_wifi_success);
                layout_wifi_starting_startWifiFragment.setVisibility(View.GONE);
                layout_wifi_restart_startWifiFragment.setVisibility(View.GONE);
                layout_wifi_success_startWifiFragment.setVisibility(View.VISIBLE);

                ObjectAnimator animator_alpha_s = ObjectAnimator.ofFloat(layout_wifi_success_startWifiFragment, "alpha", 0, 1f);
                animator_alpha_s.setDuration(1000);
                animator_alpha_s.setInterpolator(new LinearOutSlowInInterpolator());
                animator_alpha_s.start();

                break;
            default:

                break;
        }
    }
}
