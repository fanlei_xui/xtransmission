package com.xui.xtransmission.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xui.xtransmission.R;
import com.xui.xtransmission.view.ColorArcProgressBar;

public class OkTransProgressFragment extends Fragment {

    private final int FILE_TYPE_MUSIC = 0;
    private final int FILE_TYPE_APP = 1;
    private final int FILE_TYPE_MAP = 2;
    private final int FILE_TYPE_VIDEO = 5;//视频

    public interface OnOkTransProgressListener {
        void onFragmentInteraction(Uri uri);
    }

    private OnOkTransProgressListener mListener;

    public void setOnOkTransProgressListener(OnOkTransProgressListener mListener) {
        this.mListener = mListener;
    }

    public static OkTransProgressFragment newInstance() {
        OkTransProgressFragment fragment = new OkTransProgressFragment();
        return fragment;
    }

    private ColorArcProgressBar capb_progress_OkTransProgressFragment;//进度条

    private LinearLayout ll_container_OkTransProgressFragment;//容器
    private TextView tv_fileName_OkTransProgressFragment;// 显示文件名
    private ImageView iv_anim_OkTransProgressFragment;   // 动画
    private AnimationDrawable animationDrawable;

    private ImageView iv_rotate_complete_OkTransProgressFragment;//做动画的对号

    private ImageView iv_icon_OkTransProgressFragment;//文件类型

    private String tempFileName = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ok_trans_progress, container, false);

        initView(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        startAnim();

    }

    private void initView(View view) {
        capb_progress_OkTransProgressFragment = (ColorArcProgressBar) view.findViewById(R.id.capb_progress_OkTransProgressFragment);
        ll_container_OkTransProgressFragment = (LinearLayout) view.findViewById(R.id.ll_container_OkTransProgressFragment);
        tv_fileName_OkTransProgressFragment = (TextView) view.findViewById(R.id.tv_fileName_OkTransProgressFragment);
        iv_anim_OkTransProgressFragment = (ImageView) view.findViewById(R.id.iv_anim_OkTransProgressFragment);
        iv_icon_OkTransProgressFragment = (ImageView) view.findViewById(R.id.iv_icon_OkTransProgressFragment);
        iv_rotate_complete_OkTransProgressFragment = (ImageView) view.findViewById(R.id.iv_rotate_complete_OkTransProgressFragment);
    }

    private void startAnim() {
        if (animationDrawable == null) {
            animationDrawable = (AnimationDrawable) iv_anim_OkTransProgressFragment.getDrawable();
        }
        animationDrawable.start();
    }

    //设置状态
    public void setOkTransProgressStatus(int progress, int fileType, String fileName) {

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(capb_progress_OkTransProgressFragment.getCurrentAngle(), progress);
        valueAnimator.setDuration(400);
        valueAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                capb_progress_OkTransProgressFragment.setCurrentAngle((Float) valueAnimator.getAnimatedValue()); // 设置当前进度
            }
        });
        valueAnimator.start();

        startTextViewAnim(tv_fileName_OkTransProgressFragment, fileName);//开始动画

        switch (fileType) {
            case FILE_TYPE_MUSIC:// 音乐
                startImageViewAnim(iv_icon_OkTransProgressFragment, fileType, R.drawable.transmission_app_icon_type_music_01);
                break;
            case FILE_TYPE_APP:  // APP
                startImageViewAnim(iv_icon_OkTransProgressFragment, fileType, R.drawable.transmission_app_icon_type_app_01);
                break;
            case FILE_TYPE_MAP:  // 地图
                startImageViewAnim(iv_icon_OkTransProgressFragment, fileType, R.drawable.transmission_app_icon_type_map_01);
                break;
            case FILE_TYPE_VIDEO:// 视频
                startImageViewAnim(iv_icon_OkTransProgressFragment, fileType, R.drawable.transmission_app_icon_type_video_01);
                break;
            default:

                break;
        }

    }


    //文件明的动画
    private void startTextViewAnim(final TextView textView, final String fileName) {
        tempFileName = fileName;
        if (!TextUtils.isEmpty(tempFileName) && !fileName.equals(textView.getText())) {
            AnimatorSet set = new AnimatorSet();
            ObjectAnimator animator_alpha = ObjectAnimator.ofFloat(textView, "alpha", 1, 0);
            ObjectAnimator animator_trans = ObjectAnimator.ofFloat(textView, "translationY", 0, 33);
            set.setDuration(300);
            set.playTogether(animator_alpha, animator_trans);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    textView.setText(tempFileName);//设置文件名称

                    AnimatorSet set = new AnimatorSet();
                    ObjectAnimator animator_alpha = ObjectAnimator.ofFloat(textView, "alpha", 0, 1);
                    ObjectAnimator animator_trans = ObjectAnimator.ofFloat(textView, "translationY", 33, 0);
                    set.setDuration(300);
                    set.playTogether(animator_alpha, animator_trans);
                    set.setInterpolator(new AccelerateDecelerateInterpolator());
                    set.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        }
    }

    //开始图标的动画
    private void startImageViewAnim(final ImageView imageView, int fileType, final int res) {
        if (!TextUtils.isEmpty(tempFileName) && fileType != Integer.parseInt(String.valueOf(imageView.getTag()))) {
            imageView.setTag(fileType);
            AnimatorSet set = new AnimatorSet();
            ObjectAnimator animator_alpha = ObjectAnimator.ofFloat(imageView, "alpha", 1, 0);
            ObjectAnimator animator_trans = ObjectAnimator.ofFloat(imageView, "translationY", 0, -33);
            set.playTogether(animator_alpha, animator_trans);
            set.setDuration(300);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    imageView.setImageResource(res);
                    AnimatorSet set = new AnimatorSet();
                    ObjectAnimator animator_alpha = ObjectAnimator.ofFloat(imageView, "alpha", 0, 1);
                    ObjectAnimator animator_trans = ObjectAnimator.ofFloat(imageView, "translationY", -33, 0);
                    set.playTogether(animator_alpha, animator_trans);
                    set.setDuration(300);
                    set.setInterpolator(new AccelerateDecelerateInterpolator());
                    set.start();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

        }
    }


    //执行动画完成的动画
    public void setOkTransComplete() {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(capb_progress_OkTransProgressFragment.getCurrentAngle(), 100);
        valueAnimator.setDuration(400);
        valueAnimator.setInterpolator(new LinearOutSlowInInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                capb_progress_OkTransProgressFragment.setCurrentAngle((Float) valueAnimator.getAnimatedValue()); // 设置当前进度
            }
        });
        valueAnimator.start();

        //对号的动画
        final AnimatorSet iv_set = new AnimatorSet();
        ObjectAnimator anim_iv_rotation = ObjectAnimator.ofFloat(iv_rotate_complete_OkTransProgressFragment, "rotationY", 90f, 0);
        ObjectAnimator anim_iv_alpha = ObjectAnimator.ofFloat(iv_rotate_complete_OkTransProgressFragment, "alpha", 0, 1f);
        iv_set.playTogether(anim_iv_rotation, anim_iv_alpha);
        iv_set.setDuration(800);
        iv_set.setInterpolator(new AccelerateDecelerateInterpolator());
        iv_set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

            }
        });

        //布局的动画
        AnimatorSet ll_set = new AnimatorSet();
        ObjectAnimator anim_layout_rotation = ObjectAnimator.ofFloat(ll_container_OkTransProgressFragment, "rotationY", 0, -90f);
        ObjectAnimator anim_layout_alpha = ObjectAnimator.ofFloat(ll_container_OkTransProgressFragment, "alpha", 1f, 0);
        ll_set.playTogether(anim_layout_rotation, anim_layout_alpha);
        ll_set.setDuration(800);
        ll_set.setInterpolator(new AccelerateDecelerateInterpolator());
        ll_set.start();
        ll_set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ll_container_OkTransProgressFragment.setVisibility(View.GONE);
                iv_rotate_complete_OkTransProgressFragment.setVisibility(View.VISIBLE);
                iv_set.start();
            }
        });


    }
}
