package com.xui.xtransmission.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.xui.xtransmission.R;

public class TransingApErrorFragment extends Fragment {

    public interface OnTransingApErrorListener {
        void onTransingApErrorListener();
    }

    private OnTransingApErrorListener mListener;

    public void setOnTransingApErrorListener(OnTransingApErrorListener mListener) {
        this.mListener = mListener;
    }

    public static TransingApErrorFragment newInstance(Bundle bundle) {
        TransingApErrorFragment fragment = new TransingApErrorFragment();

        fragment.setArguments(bundle);

        return fragment;
    }

    private TextView tv_music_transError;// 音乐
    private TextView tv_app_transError;  // app
    private TextView tv_map_transError;  // 地图
    private TextView tv_video_transError;// 视频

    private Button btn_restart_wifi_transError;//重启WiFi

    private Bundle myData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myData = getArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_transing_ap_error, container, false);

        initView(view);
        initEvent();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    private void initView(View view) {
        tv_music_transError = (TextView) view.findViewById(R.id.tv_music_transError);
        tv_app_transError = (TextView) view.findViewById(R.id.tv_app_transError);
        tv_map_transError = (TextView) view.findViewById(R.id.tv_map_transError);
        tv_video_transError = (TextView) view.findViewById(R.id.tv_video_transError);
        btn_restart_wifi_transError = (Button) view.findViewById(R.id.btn_restart_wifi_transError);
    }

    private void initEvent() {
        btn_restart_wifi_transError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onTransingApErrorListener();
                }
            }
        });
    }

    private void initData() {
        int receiveMusic = myData.getInt("receivedMusicCount");
        int receiveApp = myData.getInt("receivedAppCount");
        int receiveMap = myData.getInt("receivedMapCount");

        if (receiveMusic != 0) {
            tv_music_transError.setText(String.valueOf(receiveMusic));
        }
        if (receiveApp != 0) {
            tv_app_transError.setText(String.valueOf(receiveApp));
        }
        if (receiveMap != 0) {
            tv_map_transError.setText(String.valueOf(receiveMap));
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
