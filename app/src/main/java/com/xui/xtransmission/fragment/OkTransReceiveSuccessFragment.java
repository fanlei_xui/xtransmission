package com.xui.xtransmission.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.xui.xtransmission.R;


public class OkTransReceiveSuccessFragment extends Fragment {

    public interface OnOkTransReceiveSuccessListener {
        void onOkTransReceiveSuccessListener();
    }

    private OnOkTransReceiveSuccessListener mListener;

    public void setOnOkTransReceiveSuccessListener(OnOkTransReceiveSuccessListener mListener) {
        this.mListener = mListener;
    }

    public static OkTransReceiveSuccessFragment newInstance(Bundle bundle) {
        OkTransReceiveSuccessFragment fragment = new OkTransReceiveSuccessFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private TextView tv_music_transSuccess;
    private TextView tv_app_transSuccess;
    private TextView tv_map_transSuccess;
    private TextView tv_video_transSuccess;

    private Button btn_continue_transSuccess;//继续传输

    private Bundle myData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myData = getArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ok_trans_receive_success, container, false);

        initView(view);
        initEvent();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initData();

    }

    private void initView(View view) {
        tv_music_transSuccess = (TextView) view.findViewById(R.id.tv_music_transSuccess);
        tv_app_transSuccess = (TextView) view.findViewById(R.id.tv_app_transSuccess);
        tv_map_transSuccess = (TextView) view.findViewById(R.id.tv_map_transSuccess);
        tv_video_transSuccess = (TextView) view.findViewById(R.id.tv_video_transSuccess);

        btn_continue_transSuccess = (Button) view.findViewById(R.id.btn_continue_transSuccess);
    }

    private void initEvent() {
        btn_continue_transSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onOkTransReceiveSuccessListener();
                }
            }
        });
    }

    private void initData() {

        int receiveMusic = myData.getInt("receivedMusicCount");
        int receiveVideo = myData.getInt("receivedVideoCount");
        int receiveApp   = myData.getInt("receivedAppCount");
        int receiveMap   = myData.getInt("receivedMapCount");

        if (receiveMusic != 0) {
            tv_music_transSuccess.setText(String.valueOf(receiveMusic));
            tv_music_transSuccess.setAlpha(1);
        }

        if (receiveVideo != 0){
            tv_video_transSuccess.setText(String.valueOf(receiveVideo));
            tv_video_transSuccess.setAlpha(1);
        }

        if (receiveApp != 0) {
            tv_app_transSuccess.setText(String.valueOf(receiveApp));
            tv_app_transSuccess.setAlpha(1);
        }

        if (receiveMap != 0) {
            tv_map_transSuccess.setText(String.valueOf(receiveMap / 2 + receiveMap % 2));
            tv_map_transSuccess.setAlpha(1);
        }

    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
