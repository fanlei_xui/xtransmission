package com.xui.xtransmission.fragment;


import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.xui.xtransmission.R;


public class NoBinddingDialogFragment extends DialogFragment implements View.OnClickListener,
        DialogInterface.OnKeyListener {

    private final boolean YES = true;
    private final boolean NO = false;


    public interface OnNoBinddingDialogListener {
        void onUnzipMapProgressListener(boolean yesOrNo);
    }

    private OnNoBinddingDialogListener mListener;

    public void setOnNoBinddingDialogListener(OnNoBinddingDialogListener mListener) {
        this.mListener = mListener;
    }

    public static NoBinddingDialogFragment newInstance() {
        NoBinddingDialogFragment fragment = new NoBinddingDialogFragment();
        return fragment;
    }

    private TextView no_bindding_title;// 标题
    private TextView tv_bindding_ok;   // 是
    private TextView tv_bindding_no;   // 否

    private String title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            if (bundle.containsKey("title")) {
                title = bundle.getString("title", "");
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.9f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setOnKeyListener(this);
        return inflater.inflate(R.layout.fragment_no_bindding_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        no_bindding_title = (TextView) view.findViewById(R.id.no_bindding_title);
        tv_bindding_ok = (TextView) view.findViewById(R.id.tv_bindding_ok);
        tv_bindding_no = (TextView) view.findViewById(R.id.tv_bindding_no);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (title != null && title.length() > 5) {
            no_bindding_title.setText(title);
        }

        tv_bindding_ok.setOnClickListener(this);
        tv_bindding_no.setOnClickListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_bindding_ok://是
                if (mListener != null) {
                    mListener.onUnzipMapProgressListener(YES);
                }
                break;
            case R.id.tv_bindding_no://否
                if (mListener != null) {
                    mListener.onUnzipMapProgressListener(NO);
                }
                break;

        }
    }
}
