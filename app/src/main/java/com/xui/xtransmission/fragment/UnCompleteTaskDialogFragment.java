package com.xui.xtransmission.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.xui.xtransmission.R;


public class UnCompleteTaskDialogFragment extends DialogFragment {

    public interface OnUnCompleteTaskListener {
        void onUnCompleteTaskListener(boolean select);
    }

    private OnUnCompleteTaskListener mListener;

    public void setOnUnCompleteTaskListener(OnUnCompleteTaskListener mListener) {
        this.mListener = mListener;
    }


    private TextView tv_uncomplete_ok; // 是
    private TextView tv_uncomplete_no; // 否

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.9f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.fragment_un_complete_task_dialog, container, false);

        initView(view);
        initEvent();

        return view;
    }

    //初始化View
    private void initView(View view) {
        tv_uncomplete_ok = (TextView) view.findViewById(R.id.tv_uncomplete_ok);
        tv_uncomplete_no = (TextView) view.findViewById(R.id.tv_uncomplete_no);
    }

    private void initEvent() {
        tv_uncomplete_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onUnCompleteTaskListener(true);//是
                }
                dismiss();
            }
        });

        tv_uncomplete_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onUnCompleteTaskListener(false);//否
                }
                dismiss();
            }
        });
    }




}
