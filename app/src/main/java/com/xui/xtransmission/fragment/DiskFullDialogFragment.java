package com.xui.xtransmission.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.xui.xtransmission.R;

public class DiskFullDialogFragment extends DialogFragment {

    public interface OnDiskFullListener {
        void onDiskFullListener();
    }

    private OnDiskFullListener mListener;

    public void setOnDiskFullListener(OnDiskFullListener mListener) {
        this.mListener = mListener;
    }

    public static DiskFullDialogFragment newInstance(Bundle bundle) {
        DiskFullDialogFragment fragment = new DiskFullDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private TextView tv_disk_full_ok;   // 确定
    private TextView tv_title_disk_full;// 标题
    private TextView tv_msg_disk_full;  // 空间

    private Bundle myData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myData = getArguments();
    }

    @Override
    public void onStart() {
        super.onStart();
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.9f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        lp.dimAmount = 0.85f;
        getDialog().getWindow().setAttributes(lp);
        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.fragment_disk_full_dialog, container, false);

        initView(view);
        initEvent();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initData();

    }

    private void initView(View view) {
        tv_disk_full_ok = (TextView) view.findViewById(R.id.tv_disk_full_ok);
        tv_title_disk_full = (TextView) view.findViewById(R.id.tv_title_disk_full);
        tv_msg_disk_full = (TextView) view.findViewById(R.id.tv_msg_disk_full);
    }

    private void initEvent() {
        tv_disk_full_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onDiskFullListener();
                }
            }
        });
    }

    private void initData() {
        tv_title_disk_full.setText(myData.getString("title"));
        if (!tv_title_disk_full.getText().toString().equals("未知错误")
                && !tv_title_disk_full.getText().toString().equals("解压错误")) {
            tv_msg_disk_full.setText(String.format(getString(R.string.unzip_error_content), myData.getInt("needSize")));
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
