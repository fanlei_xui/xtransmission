package com.xui.xtransmission.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.tcstudio.xcloudcsdk.logic.db.dao.AppDao;
import com.tcstudio.xcloudcsdk.logic.db.dao.AppendDao;
import com.tcstudio.xcloudcsdk.logic.db.dao.MapPkgDao;
import com.tcstudio.xcloudcsdk.logic.db.dao.MusicDao;
import com.tcstudio.xcloudcsdk.logic.db.dao.VideoDao;
import com.tcstudio.xcloudcsdk.logic.db.model.App;
import com.tcstudio.xcloudcsdk.logic.db.model.Append;
import com.tcstudio.xcloudcsdk.logic.db.model.AppendType_1;
import com.tcstudio.xcloudcsdk.logic.db.model.MapPkg;
import com.tcstudio.xcloudcsdk.logic.db.model.MapVoiceFile;
import com.tcstudio.xcloudcsdk.logic.db.model.Music;
import com.tcstudio.xcloudcsdk.logic.db.model.Video;
import com.tcstudio.xcloudcsdk.logic.error.Error.UNZIP_ERROR;
import com.tcstudio.xcloudcsdk.logic.manager.CopyTask;
import com.tcstudio.xcloudcsdk.logic.manager.CopyTask.CopyListener;
import com.tcstudio.xcloudcsdk.logic.manager.DiskOptimizTask;
import com.tcstudio.xcloudcsdk.logic.manager.DiskOptimizTask.OnDiskOptimizListener;
import com.tcstudio.xcloudcsdk.logic.manager.InstallTask;
import com.tcstudio.xcloudcsdk.logic.manager.InstallTask.InstallTaskListener;
import com.tcstudio.xcloudcsdk.logic.manager.TaskManager;
import com.tcstudio.xcloudcsdk.logic.manager.UnzipTask;
import com.tcstudio.xcloudcsdk.logic.manager.UnzipTask.UnzipListener;
import com.tcstudio.xcloudcsdk.logic.service.BaseService;
import com.tcstudio.xcloudcsdk.logic.util.FileUtils;
import com.tcstudio.xcloudcsdk.logic.util.LogUtil;
import com.tcstudio.xcloudcsdk.logic.util.SdCardUtils;
import com.tcstudio.xcloudcsdk.logic.util.Utils;
import com.tcstudio.xcloudcsdk.socket.listener.ServerListener;
import com.tcstudio.xcloudcsdk.socket.send.SendDataToSocket;
import com.tcstudio.xcloudcsdk.socket.server.SocketServer;
import com.tcstudio.xcloudcsdk.socket.util.CommonCfg;
import com.xui.xtransmission.utils.MediaScanner;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class CarService extends BaseService implements ServerListener {

    private static final String tag = "CarService";
    public static final int ACTION_BIND_SUCCESS = 0x00;
    public static final int ACTION_ON_OKT_RECEIVE_PROGREESS = 0x01;
    public static final int ACTION_ON_OKT_RECEIVE_START = 0x02;
    public static final int ACTION_ON_UNZIP_PROGRESS = 0x03;
    public static final int ACTION_ON_OPTIMIZ_PROGRESS = 0x04;
    public static final int ACTION_ON_OKT_RECEIVE_OVER = 0x05;
    public static final int ACTION_ON_TRANSFER_DISK_FULL = 0x06;
    public static final int ACTION_ON_UNZIP_DISK_FULL = 0x07;
    public static final int ACTION_ON_UNZIP_START = 0x08;
    public static final int ACTION_ON_UNZIP_COMPLETE = 0x09;
    public static final int ACTION_ON_UNZIP_DISK_FULL_ERROR = 0x10;
    public static final int ACTION_ON_UNZIP_OTHER_ERROR = 0x11;
    public static final int ACTION_ON_UNZIP_UNZIP_ERROR = 0x19;
    public static final int ACTION_ON_OPTIMIZ_START = 0x12;
    public static final int ACTION_ON_OPTIMIZ_SUCCESS = 0x13;
    public static final int ACTION_ON_OPTIMIZ_ERROR = 0x14;
    public static final int ACTION_ON_PROGRESS_EXCEPTION = 0x15;
    public static final int ACTION_ON_MT_RECEIVE_START = 0x16;
    public static final int ACTION_ON_MT_RECEIVE_PROGRESS = 0x17;
    public static final int ACTION_ON_MT_RECEIVE_OVER = 0x18;



    private SendDataToSocket client;
    private SocketServer server;
    private Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    protected void init() {
        super.init();
        client = SendDataToSocket.getinstance();
        startServer();
    }

    /**
     * 启动socket server
     */
    public void startServer() {
        server = SocketServer.getInstance(getApplicationContext());
        try {
            server.StartServer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭socket server
     */
    public void closeServer() {
        if (server != null) {
            try {
                server.StopServer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 执行未完成的任务
     */
    public void executeTask() {
        unzipMapPkgs();
        installApks();
        copyAppend();
    }

    /**
     * 解压地图包
     */
    private void unzipMapPkgs() {
        if (MapPkgDao.getInstance(getApplicationContext()).isHaveUnzipMapPkg()) {
            UnzipTask unzipTask = new UnzipTask(getApplicationContext(),

                    new UnzipListener() {

                        @Override
                        public void onUnzipStart() {
                            if (handler != null) {
                                handler.sendEmptyMessage(ACTION_ON_UNZIP_START);
                            }
                        }

                        @Override
                        public void onUnzipProgress(String cityName, int progress) {
                            LogUtil.i("peak", "onUnzipProgress-->" + progress);
                            if (handler != null) {
                                Message msg = handler.obtainMessage();
                                msg.what = ACTION_ON_UNZIP_PROGRESS;
                                msg.arg1 = progress;
                                msg.obj = cityName;
                                handler.sendMessage(msg);
                            }
                        }

                        @Override
                        public void onUnzipError(String cityName, int needSize, UNZIP_ERROR error) {
                            if (handler != null) {
                                if (error.equals(UNZIP_ERROR.DISK_FULL_ERROR)) {
                                    Message msg = handler.obtainMessage();
                                    msg.what = ACTION_ON_UNZIP_DISK_FULL_ERROR;
                                    msg.arg1 = needSize;
                                    msg.obj = cityName;
                                    handler.sendMessage(msg);
                                } else if (error.equals(UNZIP_ERROR.UNZIP_EXCEPTION)) {
                                    handler.sendEmptyMessage(ACTION_ON_UNZIP_UNZIP_ERROR);
                                } else {
                                    handler.sendEmptyMessage(ACTION_ON_UNZIP_OTHER_ERROR);
                                }
                            }
                        }

                        @Override
                        public void onUnzipComplete() {
                            if (handler != null) {
                                handler.sendEmptyMessage(ACTION_ON_UNZIP_COMPLETE);
                            }
                        }

                        @Override
                        public void directOptimiz(int type) {
                            executeOptimiz(type);
                        }
                    }, 1);
            TaskManager.getInstance().execute(unzipTask);
        }
    }

    /**
     * 安装apk
     */
    private void installApks() {
        LogUtil.i(tag, "installApks");
        if (AppDao.getInstance(getApplicationContext()).isHaveUninstallApp()) {
            InstallTask installTask = new InstallTask(getApplicationContext(),
                    new InstallTaskListener() {

                        @Override
                        public void onTaskComplete() {

                        }
                    });
            TaskManager.getInstance().execute(installTask);
        }
    }

    /**
     * 拷贝附加文件
     */
    private void copyAppend() {
        LogUtil.i(tag, "copyAppend");
        if (AppendDao.getInstance(getApplicationContext()).isHaveUncopyedAppend()) {
            CopyTask copyTask = new CopyTask(getApplicationContext(),
                    new CopyListener() {
                        @Override
                        public void onCopySuccess(int appendd_id, Append append) {
                            LogUtil.i(tag, "onCopySuccess");
                            AppendDao.getInstance(getApplicationContext())
                                    .updateState(appendd_id, Append.ALREADY_COPYED);
                            // 附加文件拷贝成功后删除
                            try {
                                String jsonStr = append.getAppendd_data();
                                ObjectMapper mapper = new ObjectMapper();
                                mapper.configure(
                                        JsonParser.Feature.ALLOW_SINGLE_QUOTES,
                                        true);
                                AppendType_1 appendType_1 = mapper.readValue(
                                        jsonStr, AppendType_1.class);
                                File file = new File(SdCardUtils
                                        .getAppendSaveDir()
                                        + appendType_1.getFname());
                                if (file.exists()) {
                                    file.delete();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onCopyError() {
                            LogUtil.i(tag, "onCopyError");
                        }
                    });
            TaskManager.getInstance().execute(copyTask);
        }
    }

    /**
     * 地图包迁移
     *
     * @param filePath
     */
    private DiskOptimizTask diskOptimizTask;

    private void executeOptimiz(int type) {
        LogUtil.i(tag, "executeOptimiz");
        diskOptimizTask = new DiskOptimizTask(getApplicationContext(), type,
                new OnDiskOptimizListener() {

                    @Override
                    public void onOptimizStart(long tatolFileNum) {
                        LogUtil.i(tag, "onTransferStart-开始迁移地图包");
                        if (handler != null) {
                            handler.sendEmptyMessage(ACTION_ON_OPTIMIZ_START);
                        }
                    }

                    @Override
                    public void onOptimizProgress(int progress) {
                        LogUtil.i(tag, "progress-->" + progress);
                        if (handler != null) {
                            Message msg = handler.obtainMessage();
                            msg.what = ACTION_ON_OPTIMIZ_PROGRESS;
                            msg.arg1 = progress;
                            handler.sendMessage(msg);
                        }
                    }

                    @Override
                    public void onOptimizFinish(String autonavidata70_path) {
                        LogUtil.i(tag, "onTransferFinish－地图包迁移成功");
                        if (handler != null) {
                            handler.sendEmptyMessage(ACTION_ON_OPTIMIZ_SUCCESS);
                        }
                        // 执行未完成的任务
                        executeTask();
                    }

                    @Override
                    public void onOptimizError() {
                        LogUtil.i(tag, "onTransferError-地图包迁移失败");
                        if (handler != null) {
                            handler.sendEmptyMessage(ACTION_ON_OPTIMIZ_ERROR);
                        }
                    }
                });

        TaskManager.getInstance().execute(diskOptimizTask);
    }

    /**
     * 取消地图包迁移（存储迁移）
     */
    public void cancelOptimiz() {
        if (diskOptimizTask != null) {
            diskOptimizTask.cancel();
        }
        TaskManager.getInstance().shutdownNow();
    }

    @Override
    public void onInitBindSuccess() {
        LogUtil.i(tag, "onInitBindSuccess 绑定成功");
        handler.sendEmptyMessage(ACTION_BIND_SUCCESS);
    }

    @Override
    public void onInitBindError() {
        closeServer();
    }

    //每接收完一首歌曲都会回调这个方法
    @Override
    public void onOKTReceiveMusicOver(final Music music) {
        LogUtil.i(tag, "onReceiveMusicOver-->" + music.getMusic_song_name());
        long index = MusicDao.getInstance(this).insertMusic(music);
        LogUtil.i(tag, "onOKTReceiveMusicOver-index-->" + index);
        LogUtil.i(tag, "onOKTReceiveMusicOver-path-->" + music.getMusic_path());

//        MusicExOsDao.getInstance(getApplicationContext()).insertMediaStoreAudio(music.getMusic_path());
        Log.d("fanlei_music", music.getMusic_path());
        sendRefreshMusicFileBrodcast(music.getMusic_path());

    }

    //接收单个视频文件完成
    @Override
    public void onOkTReceiveVideoOver(Video video) {
        LogUtil.d("fanlei_video", "：" + video.getVideo_file_name());
        if (video.getVideo_availability() == 1) {
            if (VideoDao.getInstance(getApplicationContext()).insertVideo(video) < 0) {//插入数据库
                VideoDao.getInstance(getApplicationContext()).upDate(video);
            }
            MediaScanner.getInstace().scanFile(getApplicationContext(), new MediaScanner.ScanFile(video.getVideo_path(), "video/mpeg"));
        } else {
            //无效文件
            FileUtils.deleteFile(video.getVideo_path());
        }
    }

    /**
     * 发送通知，告诉系统把单个音乐文件插入音频数据库
     *
     * @param filePath
     */
    private void sendRefreshMusicFileBrodcast(final String filePath) {
        MediaScanner.getInstace().scanFile(getApplicationContext(), new MediaScanner.ScanFile(filePath, "media/*"));
    }

    // 每接收完一个APP都会回调这个方法
    @Override
    public void onOKTReceiveAppOver(App app) {
        LogUtil.i(tag, "onOKTReceiveAppOver-->" + app.getApp_name()
                + "  appid-->" + app.getApp_id());
        long index = AppDao.getInstance(this).insertApp(app);
        LogUtil.i(tag, "onOKTReceiveAppOver-infdex-->" + index);
        if (index == -1) {
            AppDao.getInstance(this).updateState(app.getApp_id(), App.WAITING_INSTALL);
        }
    }

    // 每接收完一个地图都会回调这个方法
    @Override
    public void onOKTReceiveMapOver(MapPkg map) {

        LogUtil.i(tag, "onOKTReceiveMapOver-Mapd_name-->" + map.getMapd_name() + "   map_id-->" + map.getMapd_id());

        long index = MapPkgDao.getInstance(this).insertMap(map);

        LogUtil.i(tag, "onOKTReceiveMapOver-index-->" + index);

        if (index == -1) {
            MapPkgDao.getInstance(getApplicationContext()).updateState(map.getMapd_file_name(), MapPkg.WAITING_UNZIP);
        }

    }

    // 每次接收完一个地图语音文件都会回调
    @Override
    public void onOKTReceiveMapVoiceOver(MapVoiceFile mapVoiceFile) {
        LogUtil.d("fanlei", "onOKTReceiveMapVoiceOver : " + mapVoiceFile.getMapVoiceFileuri());
        //移动文件
        FileUtils.moveFile(mapVoiceFile.getMapVoiceFileuri(), SdCardUtils.getMapVoicePath());


    }

    // 每次接收完成附加文件都会回调这个方法
    @Override
    public void onOKTReceiveAppendOver(final Append append) {

        LogUtil.i(tag, "onOKTReceiveAppendOver-append_name-->" + append.getAppendd_name());

        long index = AppendDao.getInstance(getApplicationContext()).insertAppend(append);

        LogUtil.i(tag, "onOKTReceiveAppendOver-append_index-->" + index);

        if (index == -1) {
            AppendDao.getInstance(getApplicationContext()).updateState(append.getAppendd_id(), Append.WAITING_COPY);
        }

        File file = new File(append.getAppend_url());
        if (file.exists()) {
            UnzipTask unzipTask = new UnzipTask(getApplicationContext(), new UnzipListener() {
                @Override
                public void onUnzipStart() {
                    LogUtil.d("fanlei", "开始解压---地图语音图片包");
                    if (handler != null) {
                        handler.sendEmptyMessage(ACTION_ON_UNZIP_START);
                    }
                }

                @Override
                public void onUnzipProgress(String cityName, int progress) {
                    LogUtil.d("fanlei", cityName + "----" + progress);
                    if (handler != null) {
                        Message msg = handler.obtainMessage();
                        msg.what = ACTION_ON_UNZIP_PROGRESS;
                        msg.arg1 = progress;
                        msg.obj = cityName;
                        handler.sendMessage(msg);
                    }
                }

                @Override
                public void onUnzipComplete() {
                    FileUtils.deleteFile(append.getAppend_url());
                    if (handler != null) {
                        handler.sendEmptyMessage(ACTION_ON_UNZIP_COMPLETE);
                    }
                }

                @Override
                public void onUnzipError(String cityName, int needSize, UNZIP_ERROR error) {
                    LogUtil.d("fanlei", "解压错误");
                    if (handler != null) {
                        if (error.equals(UNZIP_ERROR.DISK_FULL_ERROR)) {
                            Message msg = handler.obtainMessage();
                            msg.what = ACTION_ON_UNZIP_DISK_FULL_ERROR;
                            msg.arg1 = needSize;
                            msg.obj = cityName;
                            handler.sendMessage(msg);
                        } else {
                            handler.sendEmptyMessage(ACTION_ON_UNZIP_OTHER_ERROR);
                        }
                    }
                }

                @Override
                public void directOptimiz(int type) {

                }
            }, 2);
            TaskManager.getInstance().execute(unzipTask);
        }

    }

    // 开始接收一键传输
    @Override
    public void onOKTAllReceiveStart() {
        LogUtil.i(tag, "onOKTAllReceiveStart-handler-->" + handler);
        if (handler != null) {
            handler.sendEmptyMessage(ACTION_ON_OKT_RECEIVE_START);
        }
    }

    // 接收的进度
    @Override
    public void onOKTAllReceiveProgress(String fileName, int currFileType, int progress) {
        LogUtil.i(tag, "onOKTAllReceiveProgress");
        if (handler != null) {
            Message msg = handler.obtainMessage();
            msg.what = ACTION_ON_OKT_RECEIVE_PROGREESS;
            msg.arg1 = progress;
            msg.arg2 = currFileType;
            msg.obj = fileName;
            handler.sendMessage(msg);
        }
    }

    //接收完成
    @Override
    public void onOKTAllReceiveOver(int result_type,
                                    int totalMusicCount, int receivedMusicCount,
                                    int totalVideoCount, int receivedVideoCount,
                                    int totalAppCount, int receivedAppCount,
                                    int totalMapCount, int receivedMapCount,
                                    int totalAppendCount, int receivedAppendCount,
                                    int totalTAppCount, int receivedTAppCount) {
        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (CommonCfg.OKT_RESULT.DISKFULL.ordinal() == result_type) {
                msg.what = ACTION_ON_TRANSFER_DISK_FULL;
            } else if (CommonCfg.OKT_RESULT.ERROR.ordinal() == result_type) {
                msg.what = ACTION_ON_PROGRESS_EXCEPTION;
            } else {
                msg.what = ACTION_ON_OKT_RECEIVE_OVER;
                executeTask();//执行解压地图、安装APP、等等任务
                LogUtil.d("fanlei", "全部传输完成");
            }
            Bundle bundle = new Bundle();

            bundle.putInt("totalMusicCount", totalMusicCount);
            bundle.putInt("receivedMusicCount", receivedMusicCount);

            bundle.putInt("totalVideoCount", totalVideoCount);
            bundle.putInt("receivedVideoCount", receivedVideoCount);

            bundle.putInt("totalAppCount", totalAppCount);
            bundle.putInt("receivedAppCount", receivedAppCount);

            bundle.putInt("totalMapCount", totalMapCount);
            bundle.putInt("receivedMapCount", receivedMapCount);

            bundle.putInt("totalTAppCount", totalTAppCount);
            bundle.putInt("receivedAppendCount", receivedAppendCount);

            msg.obj = bundle;
            handler.sendMessage(msg);
        }
    }

    @Override
    public void onOKTAllReceiveErr() {

    }

    @Override
    public void onServerStartSuccess() {
        LogUtil.i("peak", "onServerStartSuccess");

    }


    @Override
    public void onMTReceiveStart() {
        if (handler != null) {
            handler.sendEmptyMessage(ACTION_ON_MT_RECEIVE_START);
        }
    }

    //总进度
    @Override
    public void onMTAllReceiveProgress(String fileName, int progress) {
        if (handler != null) {
            Message msg = handler.obtainMessage();
            msg.what = ACTION_ON_MT_RECEIVE_PROGRESS;
            msg.arg1 = progress;
            msg.arg2 = 0;
            msg.obj = fileName;
            handler.sendMessage(msg);
        }
    }

    //每次接受完一首音乐
    @Override
    public void onMTReceiveOneMusicOver(Music music) {
        LogUtil.i(tag, "onReceiveMusicOver-->" + music.getMusic_song_name());
        long index = MusicDao.getInstance(this).insertMusic(music);
        LogUtil.i(tag, "onOKTReceiveMusicOver-index-->" + index);
        LogUtil.i(tag, "onOKTReceiveMusicOver-path-->" + music.getMusic_path());
        sendRefreshMusicFileBrodcast(SdCardUtils.getMusicSaveDir() + music.getMusic_file_name());
    }

    //接收音乐列表成功
    @Override
    public void onMTAllReceiveOver(int result_type, int totalMusicCount, int receivedMusicCount) {
        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (CommonCfg.OKT_RESULT.DISKFULL.ordinal() == result_type) {
                msg.what = ACTION_ON_TRANSFER_DISK_FULL;
            } else if (CommonCfg.OKT_RESULT.ERROR.ordinal() == result_type) {
                msg.what = ACTION_ON_PROGRESS_EXCEPTION;
            } else {
                msg.what = ACTION_ON_MT_RECEIVE_OVER;
            }
            Bundle bundle = new Bundle();
            bundle.putInt("totalMusicCount", totalMusicCount);
            bundle.putInt("receivedMusicCount", receivedMusicCount);
            msg.obj = bundle;
            handler.sendMessage(msg);
        }
    }


    @Override
    public void isNeedUpd(boolean isNeedUpd) {
        LogUtil.i(tag, "isNeedUpd-->" + isNeedUpd);

    }

    //开始接受升级包
    @Override
    public void onRecvTappStart() {
        if (handler != null) {
//            handler.post(new Runnable() {
//
//                @Override
//                public void run() {
//                    DialogUtil.getInstance(getApplicationContext()).showUpdateProgressDialog();
//                }
//            });
        }
    }

    //接受升级包的进度
    @Override
    public void onRecvTappProgress(final int progress, String fileName) {
        LogUtil.i(tag, "progress-->" + progress + "  fileName-->" + fileName);
        if (handler != null) {
//            handler.post(new Runnable() {
//
//                @Override
//                public void run() {
//                    DialogUtil.getInstance(getApplicationContext()).refreshProgress(progress);
//                }
//            });

        }
    }

    //接收应用升级包成功
    @Override
    public void onRecvTAppSuccess() {
        LogUtil.i(tag, "onRecvTAppSuccess");
        if (handler != null) {
//            handler.post(new Runnable() {
//
//                @Override
//                public void run() {
//                    DialogUtil.getInstance(getApplicationContext()).closeProgressDialog();
//                }
//            });
        }
        File file = new File(SdCardUtils.getTAppSaveDir());
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            File tempFile = files[i];
            Utils.installApk(getApplicationContext(), tempFile);
        }
    }

    //接收升级包出错
    @Override
    public void onRecvTAppError() {
        LogUtil.i(tag, "onRecvTAppError");
        if (handler != null) {
//            handler.post(new Runnable() {
//
//                @Override
//                public void run() {
//                    DialogUtil.getInstance(getApplicationContext()).closeProgressDialog();
//                }
//            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeServer();
    }
}
