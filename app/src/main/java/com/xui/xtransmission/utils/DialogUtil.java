package com.xui.xtransmission.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.xui.xtransmission.R;


public class DialogUtil {
	
	private Context mContext;
	private Dialog progressDialog;
	
	private static DialogUtil instance = null;
	
	public static DialogUtil getInstance(Context mContext){
		if (instance == null) {
			instance = new DialogUtil(mContext);
		}
		return instance;
	}

	private DialogUtil(Context mContext) {
		super();
		this.mContext = mContext;
	}
	
	public void showUpdateProgressDialog(){
		progressDialog = new Dialog(mContext, R.style.PopDialog);
		progressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		View view = LayoutInflater.from(mContext).inflate(R.layout.tapp_update_progress_dialog, null);
		progressDialog.setContentView(view);
		progressDialog.show();
	}
	
	public void refreshProgress(int progress){
		if (progressDialog != null) {
			ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.tapp_progress);
			progressBar.setProgress(progress);
		}
	}
	
	public void closeProgressDialog(){
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}
}
